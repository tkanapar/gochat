package gochat

import (
	"fmt"
	"io"
	"log"
	"net"
)

const address = "localhost:4001"

var partner = make(chan io.ReadWriteCloser)

func cp(a io.ReadWriteCloser, b io.ReadWriteCloser, errc chan<- error) {
	_, err := io.Copy(a, b)
	errc <- err
}

func chat(a, b io.ReadWriteCloser) {
	fmt.Fprintln(a, "Found someone say hi!")
	fmt.Fprintln(b, "Found someone say hi!")
	errc := make(chan error, 1)
	go cp(a, b, errc)
	go cp(b, a, errc)

	if err := <-errc; err != nil {
		log.Println(err)
	}

	a.Close()
	b.Close()
}

func match(c io.ReadWriteCloser) {
	fmt.Fprint(c, "Waiting for partner to Chat ...")
	select {
	case partner <- c:
	case p := <-partner:
		chat(c, p)
	}
}

func Connect() {
	l, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatal("Unable to listen on ", address)
	}
	fmt.Println("Listening on ", address)
	for {
		c, err := l.Accept()
		if err != nil {
			log.Fatal("Unable to accept data on ", address)
		}
		go match(c)
	}

}
